import FilterListIcon from '@mui/icons-material/FilterList';
import SearchIcon from '@mui/icons-material/Search';
import VisibilityIcon from '@mui/icons-material/Visibility';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Container from '@mui/material/Container';
import FormControl from '@mui/material/FormControl';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import { GetServerSideProps } from 'next';
import Link from 'next/link';
import * as React from 'react';
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
} from 'react-share';
import { FacebookIcon, LinkedinIcon, TwitterIcon } from 'react-share';

import { MainLayout } from '@/components/layout';

export const getServerSideProps: GetServerSideProps = async (context) => {
  // console.log(context);
  return { props: { id: context.query.id } };
};

// export const getStaticPaths = async () => {
//   const response = await fetch(
//     'https://portal-staging.funix.edu.vn/certificates/nfts'
//   );
//   const data = await response.json();
//   const paths = data.map((student: { hash: any }) => {
//     return {
//       params: { id: student.hash.toString() },
//     };
//   });
//   return {
//     paths,
//     fallback: false,
//   };
// };

// export const getStaticProps = async (context: { params: { id: any } }) => {
//   const id = context.params.id;

//   return {
//     props: { id },
//   };
// };
export default function Profile(id: { id: any }) {
  const [nfts, setNfts] = React.useState<any | []>([]);
  // const [images, setImages] = React.useState<any | null>([]);
  const [nftInfos, setNftInfo] = React.useState<any | []>([]);

  React.useEffect(() => {
    loadNFT(id);
  }, []);

  async function loadNFT(id: { id: any }) {
    const response = await fetch(
      'https://portal-staging.funix.edu.vn/certificates/nfts'
    );
    const data = await response.json();
    const nftList = data.filter((d: { hash: any }) => d.hash == id.id);
    setNfts(nftList);

    const ipfsImageList = await Promise.all(
      nftList.map(async (nft: { ipfs_link: RequestInfo | URL }) => {
        const ipfsEach = await fetch(nft.ipfs_link);
        const ipfsData = await ipfsEach.json();
        setNftInfo(ipfsData);
        const url = new URL(ipfsData.certificate_link);
        const host = url.host;
        const pathname = url.pathname;
        return 'https://' + host + '' + pathname + '?alt=media';
      })
    );
    // setImages(ipfsImageList);
  }

  return (
    <MainLayout>
      <div className='bg-gray-100 sm:bg-yellow-300 md:bg-green-300 lg:bg-pink-300 xl:bg-blue-300 2xl:container 2xl:mx-auto 2xl:bg-red-300'>
        <Stack direction='row'>
          <div className='h-auto w-full rounded-md bg-white shadow'>
            <div className='mx-auto h-full max-w-full bg-white p-2'>
              <div
                className='relative h-96 max-h-96 w-full rounded-lg'
                style={{
                  backgroundImage: `url('/images/cover-fb.jpg')`,
                  backgroundRepeat: 'no-repeat',
                  backgroundSize: 'cover',
                  backgroundPosition: 'center',
                }}
              >
                <div className='absolute -bottom-24 flex w-full items-center justify-center'>
                  <div className='h-52 w-52 rounded-full border-4 border-white bg-gray-300'>
                    <img
                      className='h-full w-full rounded-full'
                      src='/images/logo.jpg'
                    />
                    {/* <div className='h-full w-full rounded-full'>
                      <Image
                        src='/images/logo.jpg'
                        alt='Picture of the author'
                        layout='fill'
                      />
                    </div> */}
                  </div>
                </div>
              </div>
              <div className='mx-auto h-full max-w-5xl'>
                <div className='mt-24 flex flex-col items-center justify-center space-y-2 border-b-2 pb-3'>
                  <p className='text-4xl font-bold'>{nftInfos.name}</p>
                  <p className='text-sm text-gray-500' style={{ fontSize: 13 }}>
                    <i>
                      Chứng chỉ được cấp bởi tổ chức giáo dục trực tuyến
                      <b> Funix</b>
                    </i>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </Stack>
      </div>

      <Container sx={{ py: 1 }} maxWidth='xl' className=' bg-slate-100'>
        <Stack direction='row' spacing={2}>
          <Button>
            <FilterListIcon className='h-6' />
          </Button>

          <FormControl variant='standard' sx={{ m: 1, minWidth: 120 }}>
            <div className='relative text-gray-600 focus-within:text-gray-400'>
              <SearchIcon className='absolute mr-2 h-10 w-10 p-2' />
              <input
                type='search'
                className='w-full rounded-lg border-gray-400 py-2 pl-10 text-sm text-white focus:outline-none'
                placeholder='Search by name...'
              />
            </div>
          </FormControl>
        </Stack>
      </Container>

      <Container sx={{ py: 8 }} maxWidth='xl'>
        <Grid container spacing={4}>
          {nfts.map(
            (
              nft: {
                ipfs_link: string | any[];
                cc_jpg_link: string | any[];
                cc_name_vn: string;
              },
              i: string | number
            ) => (
              <Grid item key={i} xs={12} sm={6} md={4}>
                <Card sx={{ maxWidth: 345 }}>
                  {nft.cc_jpg_link ? (
                    <CardMedia
                      component='img'
                      image={`${nft.cc_jpg_link}`}
                      alt='random'
                    />
                  ) : (
                    <CardMedia
                      component='img'
                      image='https://source.unsplash.com/random'
                      alt='random'
                    />
                  )}
                  <CardContent>
                    <Typography gutterBottom variant='h5' component='div'>
                      {nft.cc_name_vn}
                    </Typography>
                  </CardContent>
                  <CardActions disableSpacing>
                    <IconButton aria-label='add to favorites'>
                      <Link
                        href={`/profile/${id.id}/${nft.ipfs_link.slice(34)}`}
                      >
                        <VisibilityIcon />
                      </Link>
                    </IconButton>

                    <FacebookShareButton
                      url={`${nft.cc_jpg_link}`}
                      hashtag='#funix_certificate'
                    >
                      <FacebookIcon borderRadius={5} size={32}></FacebookIcon>
                    </FacebookShareButton>
                    <TwitterShareButton
                      url={`${nft.cc_jpg_link}`}
                      hashtags={['funix', 'certificate']}
                    >
                      <TwitterIcon size={32}></TwitterIcon>
                    </TwitterShareButton>

                    <LinkedinShareButton
                      title='abc'
                      summary='abcde'
                      url={`${nft.cc_jpg_link}`}
                    >
                      <LinkedinIcon size={32}></LinkedinIcon>
                    </LinkedinShareButton>
                  </CardActions>
                </Card>
              </Grid>
            )
          )}
        </Grid>
      </Container>
    </MainLayout>
  );
}
