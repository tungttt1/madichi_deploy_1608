//Card
// import VerifiedIcon from '@mui/icons-material/Verified';
// import Box from '@mui/material/Box';
// import Typography from '@mui/material/Typography';
// import { GetServerSideProps } from 'next';
// import * as React from 'react';

// import Layout from '@/components/layout/Layout';

// interface TabPanelProps {
//   children?: React.ReactNode;
//   index: number;
//   value: number;
// }

// function TabPanel(props: TabPanelProps) {
//   const { children, value, index, ...other } = props;

//   return (
//     <div
//       role='tabpanel'
//       hidden={value !== index}
//       id={`simple-tabpanel-${index}`}
//       aria-labelledby={`simple-tab-${index}`}
//       {...other}
//     >
//       {value === index && (
//         <Box sx={{ p: 3 }}>
//           <Typography>{children}</Typography>
//         </Box>
//       )}
//     </div>
//   );
// }

// function a11yProps(index: number) {
//   return {
//     id: `simple-tab-${index}`,
//     'aria-controls': `simple-tabpanel-${index}`,
//   };
// }

// export const getServerSideProps: GetServerSideProps = async (params) => {
//   return { props: { token: params.query.token } };
// };

// export const getStaticPaths = async () => {
//   const response = await fetch(
//     'https://portal-staging.funix.edu.vn/certificates/nfts'
//   );
//   const data = await response.json();
//   const paths = data.map((student: { hash: any; ipfs_link: any }) => {
//     return {
//       params: {
//         id: student.hash.toString(),
//         token: student.ipfs_link.toString().split('/')[4],
//       },
//     };
//   });
//   return {
//     paths,
//     fallback: false,
//   };
// };

// export const getStaticProps = async (context: { params: { token: any } }) => {
//   const token = context.params.token;

//   return {
//     props: { token },
//   };
// };

// export default function Token(token: { token: any }) {
//   const [ipfs, setIpfs] = React.useState<any | null>([]);
//   const [pdf, setPdf] = React.useState<any | null>(' ');
//   const [nftToken, setNftToken] = React.useState<any | null>();
//   const [value, setValue] = React.useState(0);

//   React.useEffect(() => {
//     try {
//       loadInfo(token);
//     } catch (e) {
//       console.log(e);
//     }
//   }, []);

//   async function loadInfo(token: { token: any }) {
//     const response = await fetch(
//       `https://gateway.pinata.cloud/ipfs/${token.token}`
//     );
//     const data = await response.json();
//     const url = new URL(data.certificate_link);
//     const host = url.host;
//     const pathname = url.pathname;
//     const pdfPath = 'https://' + host + '' + pathname + '?alt=media';
//     const tokenLinkURL = new URL(data.token_link);
//     const nftToken = tokenLinkURL.search.slice(3);

//     setPdf(pdfPath);
//     setIpfs(data);
//     setNftToken(nftToken);
//   }

//   const handleChange = (event: React.SyntheticEvent, newValue: number) => {
//     setValue(newValue);
//   };

//   return (
//     <Layout>
{
  /* <section>
        <div className='grid  h-screen grid-cols-6 gap-4'>
          <div className=' col-span-4 col-start-1  '>
            <div
              className='overflow-clip border-2 border-solid border-orange-400 bg-orange-300 md:m-5 lg:m-20'
              style={{ height: 800 }}
            > */
}
// <iframe className='scrolling:no aspect-square w-full' src={pdf}></iframe>
{
  /* </div>
          </div>

          <div className='col-span-2 mt-20'>
            <p
              className='mb-10 font-medium text-cyan-500'
              style={{ fontSize: 14 }}
            > */
}
// {ipfs.certificate_name} <VerifiedIcon />
{
  /* </p>
            <h1 className='text-2xl font-bold'>
              {ipfs.certificate_name} <b>#{nftToken}</b>
            </h1>
            <div className='mb-4 mt-1 '>
              <p style={{ fontSize: 13 }}>
                <i>Được tạo ngày 02/08/2022</i>
              </p>
              <p style={{ fontSize: 15 }}>
                Sở hữu bởi: FUNiX -{' '}
                <b> */
}
// <b>{ipfs.name}</b>
{
  /* </b>
              </p>
            </div> */
}
{
  /* <Box sx={{ width: '100%' }}>
              <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs
                  value={value}
                  onChange={handleChange}
                  aria-label='basic tabs example'
                >
                  <Tab label='Thông tin' {...a11yProps(0)} />
                  <Tab label='Lịch sử' {...a11yProps(1)} disabled={true} />
                </Tabs>
              </Box>
              <TabPanel value={value} index={0}>
                <Card style={{ width: 520, marginBottom: 20 }}>
                  <CardContent>
                    <h4>Mô tả về chứng chỉ:</h4>
                    <p style={{ fontSize: 15 }}>Smart Contract:</p>
                    <p style={{ fontSize: 15 }}>
                      Địa chỉ ảnh:{' '}
                      <ArrowLink href={`${ipfs.certificate_link}`}>
                        View Image
                      </ArrowLink>
                    </p>

                    <p style={{ fontSize: 15 }}>
                      Token ID: <b>{nftToken}</b>
                    </p>
                    <p style={{ fontSize: 15 }}>
                      Định dạng Token: <b>ERC-721</b>{' '}
                    </p>
                    <p style={{ fontSize: 15 }}>
                      Blockchain: <b>Polygon</b>
                    </p>
                  </CardContent>
                </Card>

                <Card style={{ width: 520, marginBottom: 20 }}>
                  <CardContent>
                    <h4>Thông tin thêm:</h4>
                    <p style={{ fontSize: 13 }}>
                      <i>
                        Chứng chỉ được cấp bởi tổ chức giáo dục trực tuyến
                        <b> Funix</b>
                      </i>
                    </p>
                  </CardContent>
                </Card>

                <Stack spacing={2} direction='row' className='mb-5'>
                  <Button
                    variant='contained'
                    href={`https://gateway.pinata.cloud/ipfs/${token.token}`}
                  >
                    Xem trên IPFS
                  </Button>

                  <Button variant='outlined' href={`${ipfs.token_link}`}>
                    Xem trên Polyscan
                  </Button>
                </Stack>
              </TabPanel>
              <TabPanel value={value} index={1}>
                s
              </TabPanel>
            </Box>
          </div>
        </div>
      </section> */
}
//       <a href={`${ipfs.token_link}`}>Xem trên Polyscan</a>
//     </Layout>
//   );
// }
/* eslint-disable react/jsx-key */
import VerifiedIcon from '@mui/icons-material/Verified';
// import Box from '@mui/material/Box'
import Button from '@mui/material/Button';
import { GetServerSideProps } from 'next';
import * as React from 'react';

import { MainLayout } from '@/components/layout';
// import CardContent from '@mui/material/CardContent'
// import Stack from '@mui/material/Stack'
// import Tab from '@mui/material/Tab'
// import Tabs from '@mui/material/Tabs'
// import Link from 'next/link'
// import dynamic from 'next/dynamic'
// import Grid from '@mui/material/Grid'
// import CardMedia from '@mui/material/CardMedia'

export const getServerSideProps: GetServerSideProps = async (context) => {
  return { props: { token: context.query.token } };
};

export default function Token(token: { token: any }) {
  const [ipfs, setIpfs] = React.useState<any | []>([]);
  const [pdf, setPdf] = React.useState<any | []>(' ');
  const [nftToken, setNftToken] = React.useState<any | []>();
  const [value, setValue] = React.useState(0);

  React.useEffect(() => {
    try {
      loadInfo(token);
    } catch (e) {
      console.log(e);
    }
  }, []);

  async function loadInfo(token: { token: any }) {
    const response = await fetch(
      `https://gateway.pinata.cloud/ipfs/${token.token}`
    );
    const data = await response.json();
    const url = new URL(data.certificate_link);
    const host = url.host;
    const pathname = url.pathname;
    const pdfPath = 'https://' + host + '' + pathname + '?alt=media';
    const tokenLinkURL = new URL(data.token_link);
    const nftToken = tokenLinkURL.search.slice(3);

    setPdf(pdfPath);
    setIpfs(data);
    setNftToken(nftToken);
  }

  return (
    <MainLayout>
      <div className='main_container'>
        <div className='left_panel'>
          <iframe scrolling='no' src={pdf}></iframe>
        </div>
        <div className='right_panel'>
          <p className='certi_name_p' style={{ fontSize: 14 }}>
            {ipfs.certificate_name} <VerifiedIcon />
          </p>
          <h1 className='certi_name_h1'>
            {ipfs.certificate_name} <b>#{nftToken}</b>
          </h1>

          <div>
            <div className='main'>
              <div className='info'>
                <h4>Mô tả về chứng chỉ:</h4>
                <p style={{ fontSize: 15 }}>Smart Contract:</p>
                <p style={{ fontSize: 15 }}>
                  Địa chỉ ảnh:{' '}
                  <a className='certi_link' href={`${ipfs.certificate_link}`}>
                    View Image
                  </a>
                </p>

                <p style={{ fontSize: 15 }}>
                  Token ID: <b>{nftToken}</b>
                </p>
                <p style={{ fontSize: 15 }}>
                  Định dạng Token: <b>ERC-721</b>{' '}
                </p>
                <p style={{ fontSize: 15 }}>
                  Blockchain: <b>Polygon</b>
                </p>
              </div>
              <div className='info'>
                <h4>Thông tin thêm:</h4>
                <i>
                  Chứng chỉ được cấp bởi tổ chức giáo dục trực tuyến
                  <b> Funix</b>
                </i>
              </div>
              <div className='button_panel'>
                <Button
                  variant='contained'
                  href={`https://gateway.pinata.cloud/ipfs/${token.token}`}
                >
                  Xem trên IPFS
                </Button>
                <Button
                  variant='outlined'
                  href={`${ipfs.token_link}`}
                  className='btn'
                >
                  Xem trên Polyscan
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </MainLayout>
  );
}
