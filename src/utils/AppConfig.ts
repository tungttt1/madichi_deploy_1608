export const AppConfig = {
  site_name: 'FUNiX Certificate NFTs',
  title: 'FUNiX Certificate NFTs',
  description: 'Starter code for your Nextjs Boilerplate with Tailwind CSS',
  locale: 'en',
};
