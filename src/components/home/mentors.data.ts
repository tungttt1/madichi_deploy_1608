import type { Mentor } from '@/interfaces/mentor'

export const data: Array<Mentor> = [
  {
    id: 1,
    photo: '/images/mentors/mentor-Quach-Ngoc-Xuan-200x200-e1589819276288.jpg',
    name: 'Quách Ngọc Xuân',
    category: 'Phó Giám đốc FUNiX',
    description:
      'Anh Quách Ngọc Xuân – Phó Giám đốc FUNiX, nguyên Giám đốc học thuật và là một trong những “công thần” đầu tiên của Trường Mây. Trong 14 năm gia nhập đời sống công nghệ, anh Xuân đã có 8 năm kinh nghiệm giảng dạy tại FPT Aptech, Đại học FPT, Trưởng ban Phát triển chương trình FPT Polytechnic..',
  },
  {
    id: 2,
    photo: '/images/mentors/mentor-Nguyen-Tran-Phu-200x200.png',
    name: 'Trần Đình Phú',
    category: 'Business Analyst',
    description:
      'Anh Nguyễn Trần Phú tốt nghiệp Cử nhân chương trình Computer Studies, Genetic Computer School, Singapore. Hiện đang đảm nhận vai trò Senior Developer ở Tinh Vân Outsourcing, anh Phú chịu trách nhiệm triển khai các dự án quản lý quy trình kinh doanh với công nghệ Metastorm BPM... ',
  },
  {
    id: 3,
    photo: '/images/mentors/mentor-Le-Quang-Dat-200x200.jpg',
    name: 'Lê Quang Đạt',
    category: 'Technical Architect',
    description:
      'Vốn là cựu sinh viên Đại học Bách Khoa Hà Nội, ra trường anh Lê Quang Đạt đã sớm đầu quân vào FPT Software. Hiện anh Đạt đã có hơn 10 năm kinh nghiệm chuyên môn trong lĩnh vực phát triển, thực thi và triển khai ứng dụng, hỗ trợ khách hàng và hỗ trợ nội bộ; hơn 8 năm làm việc với Java... ',
  },
  {
    id: 4,
    photo: '/images/mentors/Mentor_Vũ_Đức_Quy-removebg-preview.png',
    name: 'Vũ Đức Quy',
    category: 'Solution Analyst',
    description:
      'Tốt nghiệp ngành Tin học Kinh tế, Đại học Kinh tế Quốc dân, anh Vũ Đức Quy kiên định theo đuổi ngành nghề yêu thích. Am hiểu công nghệ, nghiên cứu chuyên sâu về kinh tế, anh Quy đã tham gia nhiều dự án lớn với các doanh nghiệp và ngân hàng như: Triển khai hệ thống phân tích lợi nhuận đa chiều, Corebanking,...',
  },
]
