import React, { FC } from 'react'
import { Box, Typography } from '@mui/material'
import Image from 'next/image'

interface Props {
  onClick?: () => void
  variant?: 'primary' | 'secondary'
}

const Logo: FC<Props> = ({ onClick, variant }) => {
  return (
    <Box onClick={onClick}>
      <Typography
        variant="h4"
        component="h1"
        sx={{ fontWeight: 700, '& span': { color: variant === 'primary' ? 'primary.main' : 'unset' } }}
      >
        <span>FUNiX</span>
      </Typography>
      {/* <Image
        src="/public/images/funix-logo.png"
        layout="fill"
        sizes="(min-width: 75em) 33vw,
              (min-width: 48em) 50vw,
              100vw"
      /> */}
    </Box>
  )
}

Logo.defaultProps = {
  variant: 'primary',
}

export default Logo
