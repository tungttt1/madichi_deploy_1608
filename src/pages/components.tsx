import clsx from 'clsx';
import Link from 'next/link';
import * as React from 'react';

import { MainLayout } from '@/components/layout';
import Seo from '@/components/Seo';

export default function ComponentsPage() {
  const [listNfts, setNfts] = React.useState<any | null>([]);
  React.useEffect(() => {
    loadNFTs();
  }, []);

  async function loadNFTs() {
    const api = 'https://portal-staging.funix.edu.vn/certificates/nfts';
    const response = await fetch(api);
    const data = await response.json();

    const studentHash = await Promise.all(
      data.map((d: { hash: any }) => {
        return d.hash;
      })
    );

    const distinctStudent = studentHash.filter(
      (item, index, array) => array.indexOf(item) === index
    );
    setNfts(distinctStudent);
  }

  return (
    <MainLayout>
      <Seo
        templateTitle='List of students'
        description='Pre-built components with awesome default'
      />
      <main>
        <section>
          <div className={clsx('layout min-h-screen py-20')}>
            <div className='mt-1 space-y-6'>
              <div className='space-y-2'>
                <h2 className='text-lg md:text-xl'>List of students</h2>
                <p className={clsx('!mt-1 text-sm')}>
                  You can change primary color to any Tailwind CSS colors. See
                  globals.css to change your color.
                </p>
                <div className='flex flex-wrap gap-2'>
                  {listNfts.map((data: any, i: any) => (
                    <div key={i}>
                      <Link key={i} href={`/profile/${data}`}>
                        <div className='ml-5 mt-10'>
                          {i} - {`${data}`}
                        </div>
                      </Link>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </MainLayout>
  );
}
