import React, { FC } from 'react'
import Link from 'next/link'
import Grid from '@mui/material/Grid'
import MuiLink from '@mui/material/Link'
import type { Navigation } from '@/interfaces/navigation'
import { navigations as headerNavigations } from '@/components/navigation/navigation.data'
import { FooterSectionTitle } from '@/components/footer'

const courseMenu: Array<Navigation> = [
  {
    label: 'Giới thiệu FUNiX',
    path: 'https://funix.edu.vn/gioi-thieu-funix/ ',
  },
  {
    label: 'Đội ngũ Mentor',
    path: 'https://funix.edu.vn/mentors/',
  },
  {
    label: 'Hợp tác',
    path: 'https://funix.edu.vn/hop-tac/',
  },
  {
    label: 'Liên hệ',
    path: 'https://funix.edu.vn/lien-he/',
  },
  {
    label: 'FAQ',
    path: 'https://funix.edu.vn/faq-2/',
  },
]

const pageMenu: Array<Navigation> = [
  { label: 'Chương trình học', path: 'https://funix.edu.vn/chuong-trinh-hoc-funix/' },
  { label: 'Học phí', path: 'https://funix.edu.vn/hoc-phi/' },
  { label: 'Cách Học', path: 'https://funix.edu.vn/phuong-phap-hoc-truc-tuyen-hieu-qua/' },
  { label: 'Đời sống sinh viên', path: 'https://funix.edu.vn/doi-song-sinh-vien/' },
]

const companyMenu: Array<Navigation> = [
  { label: 'Học đường', path: 'https://funix.edu.vn/funix-activities/' },
  { label: 'Sự kiện', path: 'https://funix.edu.vn/events/' },
]

interface NavigationItemProps {
  label: string
  path: string
}

const NavigationItem: FC<NavigationItemProps> = ({ label, path }) => {
  return (
    <Link href={path} passHref>
      <MuiLink
        underline="hover"
        sx={{
          display: 'block',
          mb: 1,
          color: 'primary.contrastText',
        }}
      >
        {label}
      </MuiLink>
    </Link>
  )
}

const FooterNavigation: FC = () => {
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={4}>
        <FooterSectionTitle title="Về chúng tôi" />
        {courseMenu.map(({ label, path }, index) => (
          <NavigationItem key={index + path} label={label} path={/* path */ '#'} />
        ))}
      </Grid>
      <Grid item xs={12} md={4}>
        <FooterSectionTitle title="Học gì ở FUNiX" />
        {pageMenu.map(({ label, path }, index) => (
          <NavigationItem key={index + path} label={label} path={path} />
        ))}
      </Grid>
      <Grid item xs={12} md={4}>
        <FooterSectionTitle title="Tin tức" />
        {companyMenu.map(({ label, path }, index) => (
          <NavigationItem key={index + path} label={label} path={path} />
        ))}
      </Grid>
    </Grid>
  )
}

export default FooterNavigation
