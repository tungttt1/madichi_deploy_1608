import type { Testimonial } from '@/interfaces/testimonial'

export const data: Array<Testimonial> = [
  {
    id: 1,
    title: 'Blockchain Basics Course',
    content:
      'Lần đầu mới tiếp xúc khoá học funix 6 tháng thực sự , chỉ đọc tài liệu nó rất là khó hiểu.  nhưng học cái này mình còn xem thêm cả video trên youtube để bổ sung thêm nữa( học code tìm hiểu nhiều nguồn là chuyện bình thường) . nhưng thấy nó càng ngày càng dễ hiểu hơn, vì funix đưa mình vào lối học bài bản, đi từng bước nhỏ nhất, trên youtube chỉ cho mình làm thực hành làm theo mà thôi, hơi hạn chế giải thích. Nên mình thấy khoá học này rất ok nếu kết hợp học đọc tài liệu funix với xem video trên youtube.',
    user: {
      id: 1,
      name: 'Trần Thị Thuỳ Tùng',
      professional: 'Software Engineer',
      photo: 'tungttt.jpg',
    },
  },
  {
    id: 2,
    title: 'Smart Contract Course!',
    content:
      'Mong nhà trường cải tiến reposive trang coure funix nhà mình. em đi làm muốn dùng điện thoại để tranh thủ học thêm mà chữ nhỏ quá ạ.',
    user: {
      id: 1,
      name: 'Trần Bá Niên',
      professional: 'Blockchain Developer',
      photo: 'nientb.jpg',
    },
  },
  {
    id: 3,
    title: 'Xây dựng Giao diện Ứng dụng bằng Qt/QML',
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
    user: {
      id: 1,
      name: 'Lại Minh Sáng',
      professional: 'UX/UI Designer',
      photo: 'sanglm.jpg',
    },
  },
  {
    id: 4,
    title: 'Computer Organization and Architecture',
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
    user: {
      id: 1,
      name: 'Lê Thị Thanh Hương',
      professional: 'SEO Expert',
      photo: 'huongltt.jpg',
    },
  },
  {
    id: 5,
    title: 'Data Analysis with Python',
    content:
      'Classes that provide very detailed material in term of making UI UX Design starting team making low and hight quality, system designs, using data layout and make prototypes and testing.',
    user: {
      id: 1,
      name: 'Nguyễn Lê Anh',
      professional: 'Full-Stack Developer',
      photo: 'anhnl.jpg',
    },
  },
]
