import React, { FC } from 'react'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
// import { StyledButton } from '@/components/styled-button'

const AuthNavigation: FC = () => {
  return (
    <Box sx={{ '& button:first-child': { mr: 2 } }}>
      {/* <StyledButton disableHoverEffect={true} variant="outlined">
        Vào học 
      </StyledButton>
      <StyledButton disableHoverEffect={true}>Đăng ký</StyledButton> */}
      <Button variant="outlined" href="https://courses.funix.edu.vn/login" sx={{ mr: 2 }}>
        {' '}
        Vào học
      </Button>
      <Button variant="contained" href="https://funix.edu.vn/dang-ky-tu-van/">
        Đăng ký
      </Button>
    </Box>
  )
}

export default AuthNavigation
